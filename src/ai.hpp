#ifndef AI_HPP
#define AI_HPP

#include <vector>

#include "turn.hpp"
#include "board.hpp"

class AI
{
public:
	AI(Color::type activeColor);
	int Alfabeta(BoardModel *bm, int alfa, int beta, int counter);
	/** Zwraca najlepszą następną turę
	  */
	Turn *getNextTurn(BoardModel &bm);

private:
	static const int MAX_DEPTH = 2 * (Turn::MAX_SHIFTS + Turn::MAX_THROWS);
	/** kolor gracza który wywołał AI
	  */
	Color::type maxColor;
	/** Funkcja oceniająca.
	  * Za każdy wiersz bliżej końca +1
	  */
	int getBoardValue(BoardModel *bm);
	/** Znajduje możliwe stany planszy w odległości 1 ruchu.
	  * Przełącznik mayChange odnosi się do możliwości
	  * zmiany aktywnego gracza na planszy.
	  */
	std::vector<BoardModel*> *getSons(BoardModel &bm, bool mayChange = true);
	/** Dodaje stany planszy powstałe w wyniku
	  * przesunięcia pionka
	  */
	void addShifts(BoardModel &bm, std::vector<BoardModel*> *sons);
	/** Dodaje stany planszy powstałe w wyniku rzutu.
	  */
	void addThrows(BoardModel &bm, std::vector<BoardModel*> *sons);
	/** Dodaje ruchy z kolejnej tury.
	  * (kasuje bieżącą, zmienia aktywnego gracza,
	  * stąd kopia bm)
	  */
	void addOpponentMoves(BoardModel bm, std::vector<BoardModel*> *sons);
	/** Zwalnia pamięć */
	void deleteSons(std::vector<BoardModel*> *sons);
};

#endif // AI_HPP
