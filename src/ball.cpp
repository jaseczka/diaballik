#include <cstdlib>
#include "ball.hpp"

Ball::Ball(Color::type color, QPointF posPoint, qreal size): owner(NULL), size(size), color(color)
{
	image.load(":/img/ball");
	setPixmap(image);
	setScale(size / image.width() / 3);
	setPos(posPoint);
	setZValue(2);
}

Pawn *Ball::getOwner() const
{
	return owner;
}

Color::type Ball::getColor() const
{
	return color;
}

void Ball::setOwner(Pawn *owner)
{
	this->owner = owner;
}

void Ball::throwTo(Pawn *pawn)
{
	if (this->owner != NULL && this->owner != pawn) {
		this->owner->removeBall();
	}
	this->setOwner(pawn);
}

void Ball::animate(QPointF posPoint)
{
	QPointF endPos = posPoint;
	emit animationStarted();
	QPropertyAnimation *animation = new QPropertyAnimation(this, "position");
	connect(animation, SIGNAL(finished()), this, SLOT(onAnimationFinished()));
	animation->setStartValue(pos());
	animation->setEndValue(endPos);
	animation->setDuration(500);
	animation->setEasingCurve(QEasingCurve::InOutSine);
	animation->start(QPropertyAnimation::DeleteWhenStopped);
}

void Ball::setPos(const QPointF &pos)
{
	QGraphicsPixmapItem::setPos(makeBallPos(pos));
}

QPointF Ball::pos()
{
	QPointF QGpos = QGraphicsPixmapItem::pos();
	return QPointF(QGpos.rx() - image.width() * 4 / 3, QGpos.ry() - image.height());
}

QPointF Ball::makeBallPos(QPointF posPoint) const
{
	return QPointF(posPoint.rx() + image.width() * 4 / 3, posPoint.ry() + image.height() );
}

void Ball::onAnimationFinished()
{
	emit animationFinished();
}


