#ifndef BOARDSCENE_HPP
#define BOARDSCENE_HPP

#include <QGraphicsScene>
#include <QPainter>

class BoardScene : public QGraphicsScene
{

public:
	BoardScene(qreal size, int offsetX = 106, int offsetY = 110, int field = 54, QPixmap pixmap = QPixmap(":/img/board"), QObject *parent = NULL);
	QPixmap getPixmap() const;
	int getOffsetX() const;
	int getOffsetY() const;
	int getField() const;

private:
	void drawBackground(QPainter *painter, const QRectF &rect);
	/** Przesunięcie poziome początku planszy względem sceny
	  */
	int offsetX;
	/** Przesunięcie pionowe
	  */
	int offsetY;
	/** Wymiary pola planszy
	  */
	int field;
	QPixmap pixmap;
};

#endif // BOARDSCENE_HPP
