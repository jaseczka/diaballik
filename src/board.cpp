#include <cstdlib>
#include <QObject>
#include <QPixmap>
#include <iostream>
#include "board.hpp"
#include "boardscene.hpp"
#include "boardmodel.hpp"

Board::Board(BoardScene *scene) : boardModel(BoardModel()),
	pawnsWhite(BoardModel::SIZE), pawnsBlack(BoardModel::SIZE),
	ballWhite(NULL), ballBlack(NULL), scene(scene),
	whiteActive(true), isPawnInitialized(false),
	initializedPawn(NULL), locked(false), trackMoves(true)
{
	setupPawns(pawnsWhite, Color::WHITE);
	setupPawns(pawnsBlack, Color::BLACK);
	setupBall(ballWhite, pawnsWhite);
	setupBall(ballBlack, pawnsBlack);

	highlights = std::vector<Highlight*>(BoardModel::SIZE - 1);
	for (int i = 0; i < highlights.size(); ++i) {
		highlights[i] = new Highlight(-1, QPoint(0, 0), scene->getField());
		scene->addItem(highlights[i]);
		highlights[i]->setVisible(false);
		connect(highlights[i], SIGNAL(clicked(int)), this, SLOT(onHighlightClick(int)));
	}
}

void Board::setupBall(Ball*& ball, std::vector<Pawn*> &pawns)
{
	Pawn* owner = pawns[BoardModel::SIZE/2];
	ball = new Ball(owner->getColor(), fieldPos(owner->getField()), scene->getField());
	owner->catchBall(ball);
	scene->addItem(ball);
	connect(ball, SIGNAL(animationFinished()), this, SLOT(onAnimationFinished()));
	connect(ball, SIGNAL(animationStarted()), this, SLOT(onAnimationStarted()));
}

void Board::setupPawns(std::vector<Pawn*> &pawns, Color::type color)
{
	int buffer = 0;
	if (color == Color::BLACK)
		buffer = BoardModel::SIZE * (BoardModel::SIZE - 1);
	for (int i = 0; i < BoardModel::SIZE; ++i) {
		pawns[i] = new Pawn(i + buffer, color, fieldPos(i + buffer), scene->getField());
		scene->addItem(pawns[i]);
		connect(pawns[i], SIGNAL(clicked(int)), this, SLOT(onPawnClick(int)));
		connect(pawns[i], SIGNAL(animationFinished()), this, SLOT(onAnimationFinished()));
		connect(pawns[i], SIGNAL(animationStarted()), this, SLOT(onAnimationStarted()));
	}
}


QPointF Board::fieldPos(int field)
{
	//magic numbers związane ze specyficznym wyglądem planszy
	qreal size = scene->getField();
	qreal posX = scene->getOffsetX() + BoardModel::col(field) * (size + 1.5);
	qreal posY = scene->getOffsetY() + 5 + BoardModel::row(field) * (size + 1.5);
	return QPointF(posX, posY);
}

Turn *Board::getCurrentTurn()
{
	return boardModel.getCurrentTurn();
}

void Board::newTurn()
{
	isPawnInitialized = false;
	initializedPawn = NULL;
	clearHighlight();
	boardModel.newTurn();
	boardModel.changeActiveColor();
}

void Board::reset()
{
	if (boardModel.getActiveColor() == Color::BLACK)
		boardModel.changeActiveColor();
	resetPawns();
	delete boardModel.getCurrentTurn();
	boardModel.newTurn();
	isPawnInitialized = false;
	initializedPawn = NULL;
	trackMoves = true;
	locked = false;
	clearHighlight();
}

bool Board::isWhiteActive()
{
	return boardModel.getActiveColor() == Color::WHITE;
}

Color::type Board::getActiveColor()
{
	return boardModel.getActiveColor();
}

void Board::alterActive()
{
	boardModel.changeActiveColor();
}

void Board::clearTurn()
{
	isPawnInitialized = false;
	initializedPawn = NULL;
	boardModel.getCurrentTurn()->clear();
	clearHighlight();
}

void Board::setTrackMoves(bool track)
{
	trackMoves = track;
}

void Board::setLocked(bool locked)
{
	this->locked = locked;
}

bool Board::shiftPawn(Pawn *pawn, int field, bool animate)
{
	if (pawn == NULL) {
		std::cout << "BŁĄD: pawn = NULL" << std::endl;
		return false;
	}
	if (getPawn(field) != NULL) {
		std::cout << "BŁĄD: na polu docelowym stoi pionek" << std::endl;
		return false;
	}
	boardModel.shiftPawn(pawn->getField(), field, trackMoves);
	pawn->move(field);
	if (animate)
		pawn->animatePos(fieldPos(field));
	else
		pawn->setPos(fieldPos(field));
	return true;
}

void Board::setPawn(Pawn *pawn, int field)
{
	pawn->move(field);
	pawn->setPos(fieldPos(field));
}

bool Board::throwBall(Ball *ball, int field, bool animate)
{
	if (ball == NULL) {
		std::cerr << "BŁĄD: brak piłki" << std::endl;
		return false;
	}
	Pawn* pawn = getPawn(field);
	Pawn* owner = ball->getOwner();
	if (pawn == NULL) {
		std::cerr << "BŁĄD: brak pionka" << std::endl;
		return false;
	}
	if (pawn->getColor() != ball->getColor()) {
		std::cerr << "BŁĄD: zły kolor pionka" << std::endl;
		return false;
	}
	if (owner != NULL)
		owner->removeBall();
	if (!boardModel.throwBall(owner->getField(), field, trackMoves))
		return false;
	pawn->catchBall(ball);
	if (animate)
		ball->animate(fieldPos(field));
	else
		ball->setPos(fieldPos(field));
	return true;
}

bool Board::isTrackingMoves()
{
	return trackMoves;
}

bool Board::isWinning(Color::type color)
{
	return boardModel.isWinning(color);
}

BoardModel &Board::getBoardModel()
{
	return boardModel;
}

void Board::resetPawns()
{
	int buffer = BoardModel::SIZE * (BoardModel::SIZE - 1);
	boardModel.reset();
	for (unsigned i = 0; i < pawnsWhite.size(); ++i) {
		setPawn(pawnsWhite[i], i);
		setPawn(pawnsBlack[i], buffer + i);
	}

	setBall(ballWhite, pawnsWhite[BoardModel::SIZE/2]);
	setBall(ballBlack, pawnsBlack[BoardModel::SIZE/2]);
}

void Board::setBall(Ball *ball, Pawn *owner)
{
	ball->setPos(fieldPos(owner->getField()));
	owner->catchBall(ball);
}

void Board::onPawnClick(int field)
{
	if (locked)
		return;
	if (!isPawnInitialized) {
		initializedPawn = getPawn(field);
		if (initializedPawn->getColor() == boardModel.getActiveColor() ||
				!trackMoves) {
			isPawnInitialized = true;
			highlightMoves(initializedPawn);
		}
	} else {
		if (initializedPawn == getPawn(field)) {
			isPawnInitialized = false;
			clearHighlight();
		} else if (initializedPawn->hasBall() &&
				   boardModel.isValidThrow(initializedPawn->getField(), field) &&
				   (!trackMoves || getCurrentTurn()->getThrowsAvailable() > 0)) {
			clearHighlight();
			throwBall(initializedPawn->getBall(), field);
		} else if (!trackMoves || getPawn(field)->getColor() == initializedPawn->getColor()) {
			initializedPawn = getPawn(field);
			clearHighlight();
			highlightMoves(initializedPawn);
		}
	}
}

void Board::onHighlightClick(int field)
{
	if (locked)
		return;
	if (!isPawnInitialized || initializedPawn == NULL) {
		std::cout << "BŁĄD: Błądzący highlight";
		clearHighlight();
		return;
	}
	clearHighlight();
	shiftPawn(initializedPawn, field);
	isPawnInitialized = false;
}

void Board::onAnimationStarted()
{
	locked = true;
}

void Board::onAnimationFinished()
{
	locked = false;
	emit moveFinished();
}

Board::~Board()
{
	for (int i = 0; i < pawnsWhite.size(); ++i) {
		delete pawnsWhite[i];
		delete pawnsBlack[i];
	}
	pawnsWhite.clear();
	pawnsBlack.clear();
	foreach (Highlight *h, highlights)
		delete h;

	highlights.clear();
	delete ballWhite;
	delete ballBlack;
}

Pawn *Board::getPawn(int field) const
{
	foreach (Pawn *p, pawnsWhite) {
		if (field == p->getField())
			return p;
	}
	foreach (Pawn *p, pawnsBlack) {
		if (field == p->getField())
			return p;
	}
	return NULL;
}

Ball *Board::getBall(Color::type color)
{
	if (color == Color::WHITE)
		return ballWhite;
	return ballBlack;
}

std::vector<Pawn*> &Board::getPawns(Color::type color)
{
	if (color == Color::WHITE)
		return pawnsWhite;
	return pawnsBlack;
}

bool Board::move(int fieldFrom, int fieldTo)
{
	Pawn *pawn = getPawn(fieldFrom);
	if (pawn == NULL)
		return false;
	if (pawn->hasBall())
		return throwBall(pawn->getBall(), fieldTo);
	return shiftPawn(pawn, fieldTo);
}

void Board::setHighlight(int hIndex, int fieldToHighlight)
{
	highlights.at(hIndex)->setVisible(true);
	highlights.at(hIndex)->setPos(fieldPos(fieldToHighlight));
	highlights.at(hIndex)->setField(fieldToHighlight);
}

void Board::highlightMoves(Pawn *pawn)
{
	int h = 0;
	if (pawn->hasBall()) {
		if (trackMoves && boardModel.getCurrentTurn()->getThrowsAvailable() == 0)
			return;
		std::vector<Pawn*> pawns = getPawns(pawn->getColor());

		foreach (Pawn *p, pawns) {
			if (boardModel.isValidThrow(pawn->getField(), p->getField())) {
				setHighlight(h, p->getField());
				++h;
			}
		}
	} else {
		if (trackMoves && boardModel.getCurrentTurn()->getShiftsAvailable() <= 0)
			return;
		for (int d = 0; d < BoardModel::LAST; ++d) {
			int currentField = BoardModel::shift(pawn->getField(), BoardModel::direction(d));
			if (boardModel.isValidShift(pawn->getField(), currentField)) {
				setHighlight(h, currentField);
				++h;
			}
		}
	}
}

void Board::clearHighlight()
{
	foreach (Highlight *h, highlights) {
		h->setVisible(false);
		h->setField(-1);
	}
}
