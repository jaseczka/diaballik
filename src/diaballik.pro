#-------------------------------------------------
#
# Project created by QtCreator 2013-05-20T22:10:48
#
#-------------------------------------------------

QT       += core gui

TARGET = diaballik
TEMPLATE = app

CXX_FLAGS = -Wall
QMAKE_CXX = g++-4.7
QMAKE_CXXFLAGS += -std=c++11 -Wall

SOURCES += main.cpp\
        mainwindow.cpp \
    ball.cpp \
    pawn.cpp \
    board.cpp \
    boardscene.cpp \
    game.cpp \
    highlight.cpp \
    turn.cpp \
    player.cpp \
    humanplayer.cpp \
    computerplayer.cpp \
    ai.cpp \
    chooseplayersdialog.cpp \
    boardmodel.cpp \
    color.cpp

HEADERS  += mainwindow.hpp \
    ball.hpp \
    pawn.hpp \
    board.hpp \
    boardscene.hpp \
    game.hpp \
    highlight.hpp \
    turn.hpp \
    player.hpp \
    humanplayer.hpp \
    computerplayer.hpp \
    ai.hpp \
    chooseplayersdialog.hpp \
    color.hpp \
    boardmodel.hpp

FORMS    += mainwindow.ui \
    chooseplayersdialog.ui

RESOURCES += \
    resources.qrc
