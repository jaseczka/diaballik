#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "board.hpp"
#include <QObject>

class Player : public QObject
{
	Q_OBJECT
public:
	Player(Color::type color, bool computer);
	/** Domyślnie gra jest w trybie reagowania na kliknięcia na planszy.
	  * Gracz komputerowy zmienia to zachowanie na czas swojej tury.
	  */
	virtual void makeTurn() = 0;
	/** Czy jest graczem komputerowym?
	  */
	bool isComputer();
	Color::type getColor();
	void setColor(Color::type color);

signals:
	void turnEnded();

private slots:
	virtual void onAnimationFinished();

protected:
	Color::type color;

private:
	/** Czy jest graczem komputerowym? */
	bool computer;
};

#endif // PLAYER_HPP
