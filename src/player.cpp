#include "player.hpp"

Player::Player(Color::type color, bool isComputer) : color(color), computer(isComputer)
{
}

bool Player::isComputer()
{
	return computer;
}

Color::type Player::getColor()
{
	return color;
}

void Player::setColor(Color::type color)
{
	this->color = color;
}

void Player::onAnimationFinished()
{
}
