#include <iostream>

#include <QDebug>

#include "pawn.hpp"
#include "board.hpp"
#include "boardscene.hpp"

Pawn::Pawn(int field, Color::type color, QPointF posPoint, qreal size):
	field(field), ball(NULL), color(color), pressed(false)
{
	QPixmap image;
	if (color == Color::WHITE)
		image.load(":/img/whitepawn");
	else
		image.load(":/img/blackpawn");

	setPixmap(image);
	setScale(size/image.width());
	setPos(posPoint);
	setFlags(QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsFocusable);
	setZValue(1);
	rect = QRectF(image.rect());
}

bool Pawn::hasBall() const
{
	return ball != NULL;
}

Ball *Pawn::getBall() const
{
	return ball;
}

int Pawn::getField() const
{
	return field;
}

void Pawn::catchBall(Ball *ball)
{
	this->ball = ball;
	ball->throwTo(this);
}

void Pawn::move(int field)
{
	this->field = field;
}

void Pawn::removeBall()
{
	ball = NULL;
}

Color::type Pawn::getColor()
{
	return color;
}

void Pawn::animatePos(QPointF endPos)
{
	emit animationStarted();
	QPropertyAnimation *animation = new QPropertyAnimation(this, "position");
	connect(animation, SIGNAL(finished()), this, SLOT(onAnimationFinished()));
	animation->setStartValue(pos());
	animation->setEndValue(endPos);
	animation->setDuration(500);
	animation->setEasingCurve(QEasingCurve::InOutSine);
	animation->start(QPropertyAnimation::DeleteWhenStopped);
}

void Pawn::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	pressed = true;
	QGraphicsItem::mousePressEvent(event);
}

void Pawn::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	if(pressed)	{
		emit clicked(field);
	}
	pressed = false;
	QGraphicsItem::mouseReleaseEvent(event);
}

QPainterPath Pawn::shape() const
{
	QPainterPath p = QPainterPath(pos());
	p.addRect(rect);
	return p;

}

void Pawn::onAnimationFinished()
{
	emit animationFinished();
}

