#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QGraphicsScene>
#include <QString>
#include <QShortcut>
#include "game.hpp"
#include "boardscene.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT
    
public:
	explicit MainWindow(QWidget *parent = NULL);
	~MainWindow();
	void show();

public slots:
	/** Przycisk Nowa Gra */
	void onNewGame();
	/** Przycisk Zakończ turę
	  * lub sygnał wysłany przez gracza komputerowego.
	  * (wywołanie przez onAutoEndTurn)
	  */
	void onEndTurn();
	/** Sygnał wysłany przez piłkę lub pionek
	  */
	void onMove();
	/** Cofnij turę, Ctrl+Z */
	void onUndoTurn();
	void onUndoFinished();
	/** Ponów turę */
	void onRedoTurn();
	void onRedoFinished();
	/** Resetuj turę */
	void onResetTurn();
	/** Pomoc */
	void onHelp();
	/** Sygnał wysłany przez gracza komputerowego.
	  */
	void onAutoEndTurn();
	/** Autogra */
	void onAutoplay();
	/** Zapisz Grę */
	void onSaveGame();
	/** Wczytaj Grę
	  * Kliknięcie anuluj bądź próba otwarcia pliku
	  * o pustej nazwie nic nie robi.
	  * Jeśli nie udało się poprawnie załadować gry,
	  * następuje reset planszy.
	  */
	void onLoadGame();
	/** Edytuj planszę */
	void onEditBoard();
	/** Podpowiedź */
	void onHint();

private:
	Ui::MainWindow *ui;
	Game *game;
	BoardScene *scene;
	/** Tekst na StatusBar */
	static const QString STATUS;
	/** Czy gra jest w trybie automatycznego
	  * kończenia tury, gdy graczem aktywnym jest komputer.
	  */
	bool autoplay;
	/** Czy plansza jest w trybie edycji? */
	bool edited;
	bool redo;
	/** Skrót cofania Ctrl+Z */
	QShortcut *undoShortcut;
	void resizeEvent(QResizeEvent *e);
	void refreshStatusBar();
	/** Wywołuje okienko (dialog)
	  * w którym można wybrać typ graczy.
	  */
	bool choosePlayers(bool rejectable = true);
	void defaultSettings();
	void disableButtons();
	void makeNewTurn();
};

#endif // MAINWINDOW_HPP
