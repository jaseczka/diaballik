#ifndef BOARD_HPP
#define BOARD_HPP

#include <QGraphicsScene>
#include <QObject>
#include <vector>
#include "pawn.hpp"
#include "ball.hpp"
#include "boardscene.hpp"
#include "highlight.hpp"
#include "turn.hpp"
#include "boardmodel.hpp"

class Board : public QObject
{
	Q_OBJECT
public:
	Board(BoardScene *scene);
	~Board();
	/** Wykonuje ruch (rzut lub przesunięcie)
	  * sprawdzając czy jest legalny.
	  */
	bool move(int fieldFrom, int fieldTo);
	/** Zwraca pionek stojący na danym polu.
	  */
	Pawn *getPawn(int field) const;
	/** Zwraca piłkę danego koloru.
	  */
	Ball *getBall(Color::type color);
	/** Zwraca pionki danego koloru.
	  */
	std::vector<Pawn*> &getPawns(Color::type color);
	/** Konwerutje numer pola na pozycję na scenie.
	  */
	QPointF fieldPos(int field);
	/** Podaje ruchy z bieżącej tury.
	  */
	Turn *getCurrentTurn();
	/** Rozpoczyna nową turę na planszy:
	  * czyści podświetlenia,
	  * tworzy nowy obiekt tury,
	  * zmienia aktywny kolor.
	  */
	void newTurn();
	/** Doprowadza planszę do stanu początkowego
	  */
	void reset();
	bool isWhiteActive();
	Color::type getActiveColor();
	/** Zmienia aktywnego gracza
	  */
	void alterActive();
	/** Usuwa ruchy w bieżącej turze
	  */
	void clearTurn();
	/** Ustawia przełącznik śledzenia i
	  * sprawdzania poprawności liczby
	  * i rodzajów ruchów
	  */
	void setTrackMoves(bool track);
	/** Ustawia blokowanie elementów
	  * (ignorowanie kliknięć)
	  */
	void setLocked(bool locked);
	/** Wykonuje ruch pionkiem, sprawdzając czy jest poprawny.
	  */
	bool shiftPawn(Pawn* pawn, int field, bool animate = true);
	/** Ustawia pionek na danym polu.
	  * (nie zwraca uwagi na poprawność)
	  */
	void setPawn(Pawn* pawn, int field);
	/** Wykonuje rzut piłką, sprawdzając czy jest poprawny.
	  */
	bool throwBall(Ball* ball, int field, bool animate = true);
	/** Wręcza piłkę danemu pionkowi.
	  * (nie sprawdza poprawności)
	  */
	void setBall(Ball *ball, Pawn *owner);
	/** Czy liczba i rodzaj ruchów są śledzone?
	  */
	bool isTrackingMoves();
	/** Czy gracz danego koloru posiada układ wygrywający?
	  */
	bool isWinning(Color::type color);
	/** Zwraca model planszy
	  */
	BoardModel &getBoardModel();
	/** Usuwa pionki z planszy na "bok" (pole -1)
	  */
	void removePawns();

signals:
	void moveFinished();

public slots:
	void onPawnClick(int field);
	void onHighlightClick(int field);
	void onAnimationStarted();
	void onAnimationFinished();

private:
	BoardModel boardModel;
	std::vector<Pawn*> pawnsWhite;
	std::vector<Pawn*> pawnsBlack;
	std::vector<Highlight*> highlights;
	Ball *ballWhite;
	Ball *ballBlack;
	BoardScene *scene;
	bool whiteActive;
	/** Czy kliknięto jakiegoś pionka */
	bool isPawnInitialized;
	/** Pionek który ostatnio kliknięto */
	Pawn *initializedPawn;
	/** Zablokowanie reakcji na kliknięcia po planszy */
	bool locked;
	/** Przełącznik mówiący czy należy egzekwować
	  * ograniczenia na liczbę i rodzaj ruchów w turze.
	  */
	bool trackMoves;
	/** Podświetla pola na planszy na które jest możliwy
	  * ruch lub rzut.
	  */
	void highlightMoves(Pawn* pawn);
	/** Usuwa z planszy podświetlenia pól.
	  */
	void clearHighlight();
	/** Wręcza i tworzy piłkę środkowemu pionkowi
	  */
	void setupBall(Ball *&ball, std::vector<Pawn*> &pawns);
	/** Ustawia i tworzy pionki
	  */
	void setupPawns(std::vector<Pawn*> &pawns, Color::type color);
	/** Podświetla pole fieldToHighlight
	  */
	void setHighlight(int hIndex, int fieldToHighlight);
	/** Ustawia pionki i piłki jak na początku gry.
	  */
	void resetPawns();
};

#endif // BOARD_HPP
