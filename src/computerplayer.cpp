#include <iostream>

#include "computerplayer.hpp"

ComputerPlayer::ComputerPlayer(Color::type color, Board *board) :
	Player(color, true), board(board), ai(AI(color)), currentTurn(NULL), myTurn(false)
{
	connect(board->getBall(color), SIGNAL(animationFinished()), this, SLOT(onAnimationFinished()));
	foreach (Pawn *p, board->getPawns(color))
		connect(p, SIGNAL(animationFinished()), this, SLOT(onAnimationFinished()));
}

void ComputerPlayer::makeTurn()
{
	myTurn = true;
	board->setLocked(true);
	currentTurn = ai.getNextTurn(board->getBoardModel());
	if (currentTurn == NULL) {
		finishTurn();
		return;
	}
	currentTurn->setForwardIterator();
	if (currentTurn->hasNextMove()) {
		currentTurn->nextMove();
		board->move(currentTurn->getStartField(), currentTurn->getEndField());
	} else {
		finishTurn();
	}

}

void ComputerPlayer::onAnimationFinished()
{
	if (!myTurn)
		return;
	if (currentTurn == NULL)
		return;
	board->setLocked(true);
	if (currentTurn->hasNextMove()) {
		currentTurn->nextMove();
		board->move(currentTurn->getStartField(), currentTurn->getEndField());
	} else {
		finishTurn();
	}
}

void ComputerPlayer::finishTurn()
{
	board->setLocked(false);
	if (currentTurn != NULL)
		delete currentTurn;
	currentTurn == NULL;
	myTurn = false;
	emit turnEnded();
}
