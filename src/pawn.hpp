#ifndef PAWN_HPP
#define PAWN_HPP

#include <QGraphicsPixmapItem>

#include "color.hpp"
#include "ball.hpp"

class Ball;

class Pawn : public QObject, public QGraphicsPixmapItem
{
	Q_OBJECT
	Q_PROPERTY(QPointF position READ pos WRITE setPos)
public:
	Pawn(int field, Color::type color, QPointF posPoint, qreal size);
	/** Czy pionek ma piłkę?
	  */
	bool hasBall() const;
	/** Zwraca piłkę posiadaną przez pionka.
	  */
	Ball* getBall() const;
	/** Zwraca pole na którym stoi pionek.
	  */
	int getField() const;
	/** Odbiera piłkę poprzedniemu właścicielowi i przypisuje sobie.
	  */
	void catchBall(Ball *ball);
	/** Zmienia pole na którym stoi
	  */
	void move(int field);
	/** Usuwa wskaźnik do piłki
	  */
	void removeBall();
	/** Zwraca swój kolor.
	  */
	Color::type getColor();
	/** Animuje zmianę pozycji
	  */
	void animatePos(QPointF endPos);
	virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
	virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
	QPainterPath shape() const;

signals:
	void clicked(int);
	void animationStarted();
	void animationFinished();

private slots:
	void onAnimationFinished();

private:
	/** Pole na którym stoi */
	int field;
	Ball* ball;
	Color::type color;
	bool pressed;
	QRectF rect;
};

#endif // PAWN_HPP
