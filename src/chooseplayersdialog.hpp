#ifndef CHOOSEPLAYERSDIALOG_HPP
#define CHOOSEPLAYERSDIALOG_HPP

#include <QDialog>

namespace Ui {
class ChoosePlayersDialog;
}

class ChoosePlayersDialog : public QDialog
{
	Q_OBJECT
	
public:
	explicit ChoosePlayersDialog(QWidget *parent = NULL);
	~ChoosePlayersDialog();
	bool isWhiteHuman();
	bool isBlackHuman();
	/** Deaktywuje przycisk 'Anuluj'
	  */
	void setNotRejectable();

public slots:
	void accept();
	
private:
	Ui::ChoosePlayersDialog *ui;
	/** Rodzaj białego gracza
	  * true = człowiek
	  */
	bool whiteHuman;
	/** Rodzaj czarnego gracza
	  */
	bool blackHuman;
};

#endif // CHOOSEPLAYERSDIALOG_HPP
