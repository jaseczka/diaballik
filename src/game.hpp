#ifndef GAME_HPP
#define GAME_HPP

#include <QTextStream>

#include "board.hpp"
#include "boardscene.hpp"
#include "turn.hpp"
#include "player.hpp"
#include "computerplayer.hpp"

class Game : public QObject
{
	Q_OBJECT
public:
	Game(BoardScene *scene, bool whiteHuman = true, bool blackHuman = true);
	~Game();
	/** Kończy turę (zapisuje do historii)
	  * i inicjalizuje nową.
	  */
	void endTurn();
	/** Cofa turę.
	  */
	void undo();
	/** Ponawia turę.
	  */
	void redo();
	/** Czy cofanie jest możliwe?
	  */
	bool isUndoAvailable();
	/** Czy ponawianie jest możliwe?
	  */
	bool isRedoAvailable();
	/** Doprowadza grę do ustawień początkowych.
	  */
	void reset();
	/** Czy gracz biały jest aktywny?
	  */
	bool isWhiteActive();
	/** Zwraca liczbę dozwolonych przesunięć
	  * w bieżącej turze.
	  */
	int shiftsLeft();
	/** Zwraca liczbę dozwolonych rzutów
	  * w bieżącej turze.
	  */
	int throwsLeft();
	/** Resetuje ruchy w bieżącej turze.
	  */
	void cancelMoves();
	/** Zwraca aktywnego gracza.
	  */
	Player* getActivePlayer();
	/** Zapisuje stan gry do pliku o zadanej nazwie.
	  * W pierwszych 7miu wierszach zapisane są
	  * położenia białych pionków, w kolejnych 7miu - czarnych.
	  * Kolejny wiersz - biała piłka, kolejny - czarna.
	  * W następnych wierszach aż do końca pliku
	  * zapisane są kolejne tury. Tura składa się
	  * z max. 6 liczb oddzielonych spacjami.
	  * Para liczb reprezentuje jeden ruch.
	  * (numer pola początkowego i końcowego)
	  */
	bool writeToFile(QString fileName);
	/** Ładuje stan gry z pliku o zadanej nazwie
	  */
	bool loadFromFile(QString fileName);
	/** Ustawia rodzaj graczy w zależności
	  * od podanych przełączników.
	  */
	void setupPlayers(bool whiteHuman, bool blackHuman);
	/** Ustawia grę w trybie edycji
	  */
	void startEdit();
	/** Kończy tryb edycji.
	  */
	void finishEdit();
	/** Czy aktywny gracz posiada układ wygrywający?
	  */
	bool isActiveWinning();
	/** Wykonuje ruchy pozostałe do końca tury
	  * przy pomocy AI
	  */
	void playHint();
	/** Wywołuje odpowiedniego gracza do przeprowadzenia tury.
	  */
	void makeTurn();

signals:
	void moveFinished();
	void turnAutoEnded();
	void undoFinished();
	void redoFinished();

private slots:
	void onMoveFinished();
	void onTurnEnded();

private:
	Board *board;
	std::vector<Turn*> history;
	/** Wskazuje na miejsce za ostatnią turą.
	  * Gdy pusta historia, histIt = 0
	  */
	int historyIterator;
	/** Wskazuja na miejsce za ostatnią turą do której można ponowić.
	  */
	int historyMax;
	Player *playerWhite;
	Player *playerBlack;
	ComputerPlayer playerHint;
	bool undoing;
	bool moving;
	bool alter;
	bool hint;
	Turn *stashedTurn;
	/** Czyści historię z ruchów do których nie można dotrzeć.
	  * (np. w wyniku wykonania ruchu po serii cofnięć tur)
	  */
	void cleanHistory();
	/** Odtwarza ruchy z danej tury
	  * we wstecznym kierunku.
	  */
	void undoTurn(Turn *turn);
	/** Wczytuje pionki danego koloru ze strumienia.
	  */
	void makeMove(Turn *turn);
	bool pawnsFromStream(QTextStream &in, Color::type color);
	/** Wczytuje piłkę danego kolru ze strumienia.
	  */
	bool ballFromStream(QTextStream &in, Color::type color);
	/** Wczytuje historię ze strumienia
	  */
	bool historyFromStream(QTextStream &in);
	/** Zapisuje turę.
	  */
	void saveTurn(Turn *turn);
	void deleteHistory();
};

#endif // GAME_HPP
