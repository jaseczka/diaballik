#include <iostream>

#include <QFile>
#include <QTextStream>

#include "game.hpp"
#include "board.hpp"
#include "humanplayer.hpp"
#include "computerplayer.hpp"

Game::Game(BoardScene *scene, bool whiteHuman, bool blackHuman): board(new Board(scene)),
	history(std::vector<Turn*>()), historyIterator(0), historyMax(0),
	playerWhite(NULL), playerBlack(NULL), playerHint(Color::WHITE, board),
	undoing(false), moving(false), alter(true), hint(false), stashedTurn(NULL)
{
	setupPlayers(whiteHuman, blackHuman);
	connect(board, SIGNAL(moveFinished()), this, SLOT(onMoveFinished()));
}


void Game::setupPlayers(bool whiteHuman, bool blackHuman)
{
	if (playerWhite != NULL)
		delete playerWhite;
	if (playerBlack != NULL)
		delete playerBlack;

	if (whiteHuman)
		playerWhite = new HumanPlayer(Color::WHITE);
	else
		playerWhite = new ComputerPlayer(Color::WHITE, board);

	if (blackHuman)
		playerBlack = new HumanPlayer(Color::BLACK);
	else
		playerBlack = new ComputerPlayer(Color::BLACK, board);

	connect(playerWhite, SIGNAL(turnEnded()), this, SLOT(onTurnEnded()));
	connect(playerBlack, SIGNAL(turnEnded()), this, SLOT(onTurnEnded()));
	connect(&playerHint, SIGNAL(turnEnded()), this, SLOT(onTurnEnded()));
}

void Game::startEdit()
{
	board->setTrackMoves(false);
	board->clearTurn();
	deleteHistory();
}

void Game::finishEdit()
{
	board->setTrackMoves(true);
	board->clearTurn();
}

bool Game::isActiveWinning()
{
	return board->isWinning(getActivePlayer()->getColor());
}

void Game::playHint()
{
	hint = true;
	playerHint.setColor(board->getActiveColor());
	playerHint.makeTurn();
}

void Game::makeTurn()
{
	if (isWhiteActive())
		playerWhite->makeTurn();
	else
		playerBlack->makeTurn();
}

Game::~Game()
{
	delete board;
	delete playerWhite;
	delete playerBlack;
	deleteHistory();
}

void Game::endTurn()
{
	if (board->isTrackingMoves())
		saveTurn(board->getCurrentTurn());
	board->newTurn();
}

void Game::saveTurn(Turn *turn)
{
	if (turn != NULL) {
		cleanHistory();
		history.push_back(turn);
		++historyIterator;
		historyMax = historyIterator;
	}
}

void Game::deleteHistory()
{
	foreach (Turn *h, history)
		delete h;
	history.clear();
	historyIterator = 0;
	historyMax = 0;
}

void Game::reset()
{
	board->reset();
	deleteHistory();
}

bool Game::isWhiteActive()
{
	return board->isWhiteActive();
}

int Game::shiftsLeft()
{
	return board->getCurrentTurn()->getShiftsAvailable();
}

int Game::throwsLeft()
{
	return (board->getCurrentTurn())->getThrowsAvailable();
}

void Game::cancelMoves()
{
	alter = false;
	undoTurn(board->getCurrentTurn());
}

Player *Game::getActivePlayer()
{
	if (isWhiteActive())
		return playerWhite;
	return playerBlack;
}

bool Game::writeToFile(QString fileName)
{
	QFile file(fileName);
	if (!file.open(QIODevice::WriteOnly))
		return false;
	QTextStream out(&file);
	std::vector<Pawn*> pawnsWhite = board->getPawns(Color::WHITE);
	std::vector<Pawn*> pawnsBlack = board->getPawns(Color::BLACK);

	foreach (Pawn *p, pawnsWhite) {
		out << p->getField() << "\n";
	}
	foreach (Pawn *p, pawnsBlack) {
		out << p->getField() << "\n";
	}

	out << board->getBall(Color::WHITE)->getOwner()->getField() <<"\n";
	out << board->getBall(Color::BLACK)->getOwner()->getField() <<"\n";

	foreach (Turn *turn, history) {
		turn->setForwardIterator();
		while(turn->hasNextMove()) {
			turn->nextMove();
			out << turn->getStartField() << " " << turn->getEndField() << " ";
		}
		out << "\n";
	}

	file.close();
	return true;
}

bool readField(const QString &instr, int &field)
{
	bool ok;
	field = instr.toInt(&ok);
	if (!ok)
		return false;
	if (field < 0 || field >= BoardModel::SIZE * BoardModel::SIZE)
		return false;
	return true;
}

bool readField(QTextStream &in, int &field)
{
	QString instr;
	instr = in.readLine(3);
	return readField(instr, field);
}

bool Game::pawnsFromStream(QTextStream &in, Color::type color)
{

	int i = 0;
	int field;
	std::vector<Pawn*> pawns = board->getPawns(color);
	int *pawnsModel = board->getBoardModel().getPawns(color);

	while (i < BoardModel::SIZE && !in.atEnd()) {
		if (!readField(in, field)) {
			std::cout << "BŁĄD: nie powiodło się czytanie pola" << std::endl;
			return false;
		}
		if (board->getPawn(field) != NULL){
			std::cout << "BŁĄD: na polu " << field << " stoi już pionek" << std::endl;
			return false;
		}
		pawnsModel[i] = field;
		board->setPawn(pawns[i], field);

		++i;
	}
	if (i != BoardModel::SIZE) {
		std::cout << "BŁĄD: za mało pionków" << std::endl;
		return false;
	}
	return true;
}

bool Game::ballFromStream(QTextStream &in, Color::type color)
{
	int field;
	if (!readField(in, field))
		return false;

	Pawn* owner = board->getPawn(field);
	if (owner == NULL) {
		std::cout << "BŁĄD: brak pionka" << std::endl;
		return false;
	}

	if (owner->getColor() != color) {
		std::cout << "BŁĄD: zły kolor pionka" << std::endl;
		return false;
	}

	board->setBall(board->getBall(color), owner);
	return true;
}

bool Game::historyFromStream(QTextStream &in)
{
	deleteHistory();
	historyIterator = 0;
	QString strline;
	QStringList moves;
	int fieldFrom;
	int fieldTo;
	int maxMoves = Turn::MAX_SHIFTS + Turn::MAX_THROWS;
	while (!in.atEnd()) {
		strline = in.readLine(9 * maxMoves);
		if (strline.isEmpty())
			break;
		strline = strline.trimmed();
		moves = strline.split(" ");
		if (moves.size() > 2 * maxMoves) {
			std::cout << "BŁĄD: za dużo ruchów w turze" << std::endl;
			return false;
		}
		if (moves.size() % 2 != 0) {
			std::cout << "BŁĄD: niekompletny ruch" << std::endl;
			return false;
		}
		Turn* turn = new Turn();
		for (int i = 0; i < moves.size(); i += 2) {
			if (!readField(moves.at(i), fieldFrom)) {
				std::cout << "BŁĄD: nie udało się wczytać pola z" << std::endl;
				delete turn;
				return false;
			}
			if (!readField(moves.at(i + 1), fieldTo)) {
				std::cout << "BŁĄD: nie udało się wczytać pola do" << std::endl;
				delete turn;
				return false;
			}
			turn->addMove(fieldFrom, fieldTo);
		}
		history.push_back(turn);
		++historyIterator;
	}
	historyMax = historyIterator;
	//jeśli histIt nieparzysty powinnien być biały, wpp czarny.
	if ((historyIterator % 2 == 1) != isWhiteActive())
			board->alterActive();
	return true;
}

void removePawns(std::vector<Pawn*> &pawns)
{
	foreach (Pawn *p, pawns)
		p->move(-1);
}

bool Game::loadFromFile(QString fileName)
{
	QFile file(fileName);
	if (!file.open(QIODevice::ReadOnly))
		return false;
	QTextStream in(&file);

	removePawns(board->getPawns(Color::WHITE));
	removePawns(board->getPawns(Color::BLACK));

	if (!pawnsFromStream(in, Color::WHITE)) {
		file.close();
		return false;
	}

	if (!pawnsFromStream(in, Color::BLACK)) {
		file.close();
		return false;
	}

	if (!ballFromStream(in, Color::WHITE)) {
		file.close();
		return false;
	}

	if (in.atEnd()) {
		file.close();
		return false;
	}

	if (!ballFromStream(in, Color::BLACK)) {
		file.close();
		return false;
	}

	if (!historyFromStream(in)) {
		file.close();
		return false;
	}

	file.close();

	board->newTurn();
	return true;
}

void Game::undo()
{
	if (isUndoAvailable()) {
		--historyIterator;
		undoTurn(history.at(historyIterator));
	}
}

void Game::redo()
{
	if (isRedoAvailable()) {
		board->setTrackMoves(false);
		Turn *turn = history.at(historyIterator);
		++historyIterator;
		turn->setForwardIterator();
		makeMove(turn);
	}
}

bool Game::isUndoAvailable()
{
	return historyIterator > 0;
}

bool Game::isRedoAvailable()
{
	return historyIterator < historyMax;
}

void Game::onMoveFinished()
{
	if (board->isTrackingMoves() && !hint)
		emit moveFinished();
	if (moving)
		makeMove(stashedTurn);
}

void Game::onTurnEnded()
{
	hint = false;
	emit turnAutoEnded();
}

void Game::cleanHistory()
{
	while (historyMax > historyIterator) {
		--historyMax;
		delete history.back();
		history.pop_back();
	}
}

void Game::undoTurn(Turn *turn)
{
	board->setTrackMoves(false);
	turn->setBackwardIterator();
	undoing = true;
	makeMove(turn);
}

void Game::makeMove(Turn *turn)
{
	if(turn->hasNextMove()) {
		moving = true;
		stashedTurn = turn;
		turn->nextMove();
		if (undoing)
			board->move(turn->getEndField(), turn->getStartField());
		else
			board->move(turn->getStartField(), turn->getEndField());
	} else {
		stashedTurn = NULL;
		moving = false;
		board->setTrackMoves(true);
		if (alter)
			board->alterActive();
		else //to był reset tury
			board->clearTurn();
		if (undoing)
			emit undoFinished();
		else
			emit redoFinished();
		undoing = false;
		alter = true;
	}
}


