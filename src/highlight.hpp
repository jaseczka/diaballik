#ifndef HIGHLIGHT_HPP
#define HIGHLIGHT_HPP

#include <QObject>
#include <QGraphicsRectItem>
#include <QPainterPath>

class Highlight : public QObject, public QGraphicsRectItem
{
	Q_OBJECT
public:
	explicit Highlight(int field, QPointF posPoint, int size, QObject *parent = NULL);
	QRectF boundingRect() const;
	QPainterPath shape() const;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem * /* *option */, QWidget * /* *widget */);
	void setField(int field);
	int getField();
	
protected:
	void mousePressEvent(QGraphicsSceneMouseEvent *event);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

signals:
	void clicked(int);
	
public slots:

private:
	/** Numer pola */
	int field;
	/** Wymiar podświetlenia */
	int size;
	bool pressed;
	QRectF rect;
	bool isVisible;
};

#endif // HIGHLIGHT_HPP
