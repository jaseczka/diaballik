#include <cstdlib>
#include <algorithm>
#include <array>
#include <iostream>

#include "boardmodel.hpp"

BoardModel::BoardModel() : activeColor(Color::WHITE), currentTurn(new Turn())
{
	reset();
}

BoardModel::BoardModel(const BoardModel &bm) :
	ballWhiteOwnerIndex(bm.ballWhiteOwnerIndex),
	ballBlackOwnerIndex(bm.ballBlackOwnerIndex),
	activeColor(bm.activeColor),
	currentTurn(new Turn(*bm.currentTurn))
{
	for (int i = 0; i < 2 * SIZE; ++i) {
		pawns[i] = bm.pawns[i];
	}
}

BoardModel::~BoardModel()
{
	delete currentTurn;
}

int BoardModel::row(int field)
{
	if (field >= 0 && field < SIZE*SIZE) {
		return field / SIZE;
	}

	return -1;
}

int BoardModel::col(int field)
{
	if (field >= 0 && field < SIZE*SIZE) {
		return field % SIZE;
	}

	return -1;
}

int BoardModel::field(int row, int col)
{
	if (row >= 0 && row < SIZE && col >= 0 && col < SIZE) {
		return row * SIZE + col;
	}

	return -1;

}

int BoardModel::shift(int fieldFrom, direction d)
{
	switch(d) {
	case UP:
		return field(row(fieldFrom)-1, col(fieldFrom));
	case DOWN:
		return field(row(fieldFrom)+1, col(fieldFrom));
	case LEFT:
		return field(row(fieldFrom), col(fieldFrom)-1);
	case RIGHT:
		return field(row(fieldFrom), col(fieldFrom)+1);
	default:
		return -1;
	}
}

bool BoardModel::move(int fieldFrom, int fieldTo)
{
	int pawnIndex = getPawnIndex(fieldFrom);
	if (pawnIndex < 0)
		return false;
	if (fieldFrom == pawns[ballWhiteOwnerIndex] ||
			fieldFrom == pawns[ballBlackOwnerIndex])
		return throwBall(fieldFrom, fieldTo);
	return shiftPawn(fieldFrom, fieldTo);
}

bool BoardModel::throwBall(int fieldFrom, int fieldTo, bool track)
{
	//sprawdza czy są pionki na polach i czy są na linii.
	if (!isValidThrow(fieldFrom, fieldTo))
		return false;
	if (getPawnIndex(fieldFrom) < SIZE)
		ballWhiteOwnerIndex = getPawnIndex(fieldTo);
	else
		ballBlackOwnerIndex = getPawnIndex(fieldTo);
	if (track)
		currentTurn->addThrow(fieldFrom, fieldTo);
	return true;
}

bool BoardModel::shiftPawn(int fieldFrom, int fieldTo, bool track)
{
	if (!isValidShift(fieldFrom, fieldTo))
		return false;
	pawns[getPawnIndex(fieldFrom)] = fieldTo;
	if (track)
		currentTurn->addShift(fieldFrom, fieldTo);
	return true;
}

int BoardModel::nextField(int fieldStart, int rows, int cols)
{
	return field(row(fieldStart) + rows, col(fieldStart) + cols);
}


int sgn(int a)
{
	if (a > 0) return 1;
	if (a < 0) return -1;
	return 0;
}


bool BoardModel::isValidThrow(int fieldFrom, int fieldTo)
{
	if (fieldFrom < 0 || fieldFrom >= SIZE * SIZE ||
			fieldTo < 0 || fieldTo >= SIZE * SIZE)
		return false;
	int pawnFromIndex = getPawnIndex(fieldFrom);
	int pawnToIndex = getPawnIndex(fieldTo);
	if (pawnFromIndex < 0 || pawnToIndex < 0)
		return false;
	if (fieldFrom == fieldTo)
		return false;
	if (!areTheSameColor(pawnFromIndex, pawnToIndex))
		return false;
	int rowDiff = row(fieldTo) - row(fieldFrom);
	int colDiff = col(fieldTo) - col(fieldFrom);
	if (rowDiff == 0 || colDiff == 0 || abs(rowDiff) == abs(colDiff)) {
		int currentField = nextField(fieldFrom, sgn(rowDiff), sgn(colDiff));
		while (currentField != fieldTo) {
			if (getPawnIndex(currentField) >= 0 &&
					!areTheSameColor(pawnFromIndex, getPawnIndex(currentField)))
				return false;
			currentField = nextField(currentField, sgn(rowDiff), sgn(colDiff));
		}
		return true;
	}
	return false;
}

bool BoardModel::isValidShift(int fieldFrom, int fieldTo)
{
	if (fieldFrom < 0 || fieldFrom >= SIZE * SIZE ||
			fieldTo < 0 || fieldTo >= SIZE * SIZE)
		return false;
	if (getPawnIndex(fieldFrom) < 0 || getPawnIndex(fieldTo) >= 0)
		return false;
	if (getPawnIndex(fieldFrom) == ballWhiteOwnerIndex ||
			getPawnIndex(fieldFrom) == ballBlackOwnerIndex)
		return false;
	if ((row(fieldFrom) == row(fieldTo) && abs(col(fieldFrom) - col(fieldTo)) == 1) ||
			(col(fieldFrom) == col(fieldTo) && abs(row(fieldFrom) - row(fieldTo)) == 1))
		return true;
	return false;
}

bool BoardModel::isWinning(Color::type color)
{	
	if (color == Color::WHITE) {
		return isWinning(0, SIZE, SIZE, 2 * SIZE, DOWN, ballWhiteOwnerIndex);
	} else {
		return isWinning(SIZE, 2 * SIZE, 0, SIZE, UP, ballBlackOwnerIndex);
	}
}

bool BoardModel::isWinning(int thisStart, int thisEnd, int otherStart, int otherEnd, BoardModel::direction d, int ballOwner)
{
	//sprawdzanie warunku ściany
	int count = 0;
	for (int i = thisStart; i < thisEnd; ++i) {
		int index = getPawnIndex(shift(pawns[i], d));
		if (index >= otherStart && index < otherEnd)
			++count;
	}

	if (count >= 3) {
		bool ok = true;
		int i = otherStart + 1;
		int f = pawns[otherStart];
		while (ok && i < otherEnd) {
			//jeśli 3 sąsiednie pola są wolne lub białe to nie ma ściany
			int up = getPawnIndex(field(row(f) - 1, col(f) + 1));
			int same = getPawnIndex(field(row(f), col(f) + 1));
			int down = getPawnIndex(field(row(f) + 1, col(f) + 1));
			if (up >= otherStart && up < otherEnd) {
				++i;
				f = pawns[up];
			} else if (same >= otherStart && same < otherEnd) {
				++i;
				f = pawns[same];
			} else if (down >= otherStart && down < otherEnd) {
				++i;
				f = pawns[down];
			} else {
				ok = false;
			}
		}
		if (ok)
			return true;
	}
	//sprawdzanie warunku piłki na końcu
	if (ballOwner >= thisEnd || ballOwner < thisStart)
		return false;
	if (pawns[ballOwner] < otherStart * (thisEnd - 1))
		return false;
	if (pawns[ballOwner] >= otherStart * (thisEnd - 1) + SIZE)
		return false;
	return true;
}

Turn *BoardModel::getCurrentTurn()
{
	return currentTurn;
}

void BoardModel::newTurn()
{
	currentTurn = new Turn();
}

Color::type BoardModel::getActiveColor()
{
	return activeColor;
}

void BoardModel::changeActiveColor()
{
	if (activeColor == Color::WHITE) {
		activeColor = Color::BLACK;
	} else {
		activeColor = Color::WHITE;
	}
}

int *BoardModel::getPawns()
{
	return pawns;
}

int *BoardModel::getPawns(Color::type color)
{
	if (color == Color::WHITE)
		return pawns;
	return pawns + SIZE;
}

int BoardModel::getBallField(Color::type color)
{
	if (color == Color::WHITE)
		return pawns[ballWhiteOwnerIndex];
	return pawns[ballBlackOwnerIndex];
}

void BoardModel::setWhiteBallOwner(int i)
{
	ballWhiteOwnerIndex = i;
}

void BoardModel::setBlackBallOwner(int i)
{
	ballBlackOwnerIndex = SIZE + i;
}

void BoardModel::reset()
{
	int buffer = SIZE * (SIZE - 1);
	for (int i = 0; i < SIZE; i++) {
		pawns[i] = i;
		pawns[SIZE + i] = buffer + i;
	}
	ballWhiteOwnerIndex = SIZE/2;
	ballBlackOwnerIndex = SIZE + SIZE/2;
}

int BoardModel::getPawnIndex(int field)
{
	for (int i = 0; i < 2 * SIZE; i++) {
		if (field == pawns[i])
			return i;
	}
	return -1;
}

bool BoardModel::areTheSameColor(int index1, int index2)
{
	if (index1 < 0 || index2 < 0)
		return false;
	return (index1 < SIZE && index2 < SIZE) ||
			(index1 >= SIZE && index2 >= SIZE);
}
