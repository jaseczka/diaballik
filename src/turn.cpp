#include <iostream>

#include "turn.hpp"

Turn::Turn() : iterator(-2), fields(std::vector<int>()),
	shifts(0), throws(0), forward(true)
{

}

Turn::~Turn()
{
}

void Turn::setForwardIterator()
{
	iterator = -2;
	forward = true;
}

void Turn::setBackwardIterator()
{
	forward = false;
	iterator = fields.size();
}

int Turn::getStartField()
{
	if (iterator >= 0 && iterator < fields.size() - 1)
		return fields[iterator];
	return -1;
}

int Turn::getEndField()
{
	if (iterator >= 0 && iterator < fields.size() - 1)
		return fields[iterator + 1];
	return -1;
}

void Turn::nextMove()
{
	if (forward)
		iterator += 2;
	else
		iterator -= 2;
}

bool Turn::hasNextMove()
{
	if (forward)
		return fields.size() > iterator + 2;
	return iterator > 0;
}

void Turn::addShift(int fieldFrom, int fieldTo)
{
		addMove(fieldFrom, fieldTo);
		++shifts;
}

void Turn::addThrow(int fieldFrom, int fieldTo)
{
		addMove(fieldFrom, fieldTo);
		++throws;
}

int Turn::getShiftsAvailable()
{
	return MAX_SHIFTS - shifts;
}

int Turn::getThrowsAvailable()
{
	return MAX_THROWS - throws;
}

void Turn::clear()
{
	fields.clear();
	shifts = 0;
	throws = 0;
	setForwardIterator();
}

void Turn::addMove(int fieldFrom, int fieldTo)
{
	fields.push_back(fieldFrom);
	fields.push_back(fieldTo);
}


