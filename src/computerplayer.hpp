#ifndef COMPUTERPLAYER_HPP
#define COMPUTERPLAYER_HPP

#include "player.hpp"
#include "ai.hpp"
#include <QObject>

class ComputerPlayer : public Player
{
public:
	ComputerPlayer(Color::type color, Board *board);
	/** Wykonuje ruch używając AI.
	  * Na koniec tury wysyła sygnał turnEnded()
	  */
	void makeTurn();

public slots:
	void onAnimationFinished();

private:
	Board* board;
	AI ai;
	/** Wskaźnik pobierany od AI
	  */
	Turn *currentTurn;
	bool myTurn;
	void finishTurn();
};

#endif // COMPUTERPLAYER_HPP
