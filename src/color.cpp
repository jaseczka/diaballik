#include "color.hpp"

Color::type Color::other(Color::type color)
{
	if (color == WHITE)
		return BLACK;
	return WHITE;
}
