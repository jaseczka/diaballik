#include <iostream>

#include <QDebug>
#include <QPainter>
#include <cstdlib>
#include <QTextEdit>
#include <QFile>
#include <QTextStream>
#include <QFileDialog>
#include <QMessageBox>
#include <QShortcut>

#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include "boardscene.hpp"
#include "board.hpp"
#include "game.hpp"
#include "chooseplayersdialog.hpp"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
	ui(new Ui::MainWindow), autoplay(false), edited(false), redo(false)
{
	ui->setupUi(this);
	scene = new BoardScene(600);
	game = new Game(scene);
	undoShortcut = new QShortcut(QKeySequence("Ctrl+Z"), this);
	connect(game, SIGNAL(moveFinished()), this, SLOT(onMove()));
	connect(game, SIGNAL(turnAutoEnded()), this, SLOT(onAutoEndTurn()));
	connect(game, SIGNAL(undoFinished()), this, SLOT(onUndoFinished()));
	connect(game, SIGNAL(redoFinished()), this, SLOT(onRedoFinished()));
	connect(undoShortcut, SIGNAL(activated()), this, SLOT(onUndoTurn()));
	ui->graphicsView->setScene(scene);
	resize(700, 650);
	refreshStatusBar();
	defaultSettings();
}

void MainWindow::resizeEvent(QResizeEvent *e)
{
	ui->graphicsView->fitInView(ui->graphicsView->sceneRect(), Qt::KeepAspectRatio);
	QMainWindow::resizeEvent(e);
}

const QString MainWindow::STATUS = QString::fromUtf8("Aktywny: %1; Ruchów: %2; Rzutów: %3.");

void MainWindow::refreshStatusBar()
{
	QString player;
	if (game->isWhiteActive())
		player = QString::fromUtf8("Biały");
	else
		player = QString::fromUtf8("Czarny");
	statusBar()->showMessage(STATUS.arg(player).arg(game->shiftsLeft()).arg(game->throwsLeft()));
}

void MainWindow::show()
{
	QMainWindow::show();
	ui->graphicsView->fitInView(ui->graphicsView->sceneRect(), Qt::KeepAspectRatio);
}

bool MainWindow::choosePlayers(bool rejectable) {
	ChoosePlayersDialog *dialog = new ChoosePlayersDialog(this);
	if (!rejectable)
		dialog->setNotRejectable();
	if (dialog->exec() == QDialog::Rejected)
		return false;
	game->setupPlayers(dialog->isWhiteHuman(), dialog->isBlackHuman());
	ui->endTurnButton->setDisabled(game->getActivePlayer()->isComputer());
	return true;
}

void MainWindow::defaultSettings()
{
	autoplay = false;
	edited = false;
	ui->undoTurnButton->setDisabled(true);
	ui->redoTurnButton->setDisabled(true);
	ui->resetTurnButton->setDisabled(true);
	ui->saveGameButton->setDisabled(true);
	ui->endTurnButton->setDisabled(true);
	ui->autoplayButton->setChecked(false);
	undoShortcut->blockSignals(true);
	ui->hintButton->setDisabled(game->getActivePlayer()->isComputer());
	ui->editBoardButton->setText(QString::fromUtf8("&Edytuj Grę"));
	refreshStatusBar();
}

void MainWindow::disableButtons()
{
	ui->undoTurnButton->setDisabled(true);
	ui->redoTurnButton->setDisabled(true);
	ui->endTurnButton->setDisabled(true);
	ui->resetTurnButton->setDisabled(true);
	ui->hintButton->setDisabled(true);
	ui->editBoardButton->setDisabled(true);
	ui->saveGameButton->setDisabled(true);
}

void MainWindow::makeNewTurn()
{
	game->makeTurn();

	ui->resetTurnButton->setDisabled(true);
	ui->endTurnButton->setDisabled(true);
	if (game->getActivePlayer()->isComputer()) {
		ui->undoTurnButton->setDisabled(true);
		undoShortcut->blockSignals(true);
		ui->hintButton->setDisabled(true);
		ui->editBoardButton->setDisabled(true);
	} else {
		ui->undoTurnButton->setEnabled(true);
		undoShortcut->blockSignals(false);
		ui->saveGameButton->setEnabled(true);
		ui->editBoardButton->setEnabled(true);
		ui->hintButton->setEnabled(true);
	}
}

void MainWindow::onNewGame()
{
	if (!choosePlayers())
		return;
	game->reset();
	game->makeTurn();
	defaultSettings();
}

void MainWindow::onEndTurn()
{
	game->endTurn();
	refreshStatusBar();
	if (edited)
		return;
	makeNewTurn();
}

void MainWindow::onMove()
{
	if (edited)
		return;

	refreshStatusBar();
	ui->redoTurnButton->setDisabled(true);
	ui->undoTurnButton->setDisabled(true);
	undoShortcut->blockSignals(true);
	ui->saveGameButton->setDisabled(true);
	ui->editBoardButton->setDisabled(true);

	if (game->isActiveWinning()) {
		ui->endTurnButton->setDisabled(true);
		ui->resetTurnButton->setDisabled(true);
		QString winMsg;
		if (game->isWhiteActive())
			winMsg = QString::fromUtf8("Biały");
		else
			winMsg = QString::fromUtf8("Czarny");
		QMessageBox::information(this, QString::fromUtf8("Gratulacje"), QString::fromUtf8("%1 Gracz wygrał.").arg(winMsg));
		return;
	}
	// jeśli człowiek
	if (!game->getActivePlayer()->isComputer()) {
		ui->resetTurnButton->setEnabled(true);
		ui->endTurnButton->setEnabled(true);
		ui->hintButton->setDisabled(true);
		ui->editBoardButton->setEnabled(true);
		return;
	}
}

void MainWindow::onUndoTurn()
{
	game->undo();
	disableButtons();
	refreshStatusBar();
}

void MainWindow::onUndoFinished()
{
	ui->hintButton->setEnabled(true);
	ui->editBoardButton->setEnabled(true);
	ui->undoTurnButton->setEnabled(game->isUndoAvailable());
	undoShortcut->blockSignals(!game->isUndoAvailable());
	ui->saveGameButton->setEnabled(game->isUndoAvailable());
	ui->redoTurnButton->setEnabled(game->isRedoAvailable());
	refreshStatusBar();
}

void MainWindow::onRedoTurn()
{
	game->redo();
	disableButtons();
	refreshStatusBar();
}

void MainWindow::onRedoFinished()
{
	onUndoFinished();
	game->makeTurn();
}

void MainWindow::onResetTurn()
{
	game->cancelMoves();
	ui->resetTurnButton->setDisabled(true);
	ui->endTurnButton->setDisabled(true);
	refreshStatusBar();
}

void MainWindow::onHelp()
{
	QTextEdit *help = new QTextEdit(this);
	help->setWindowFlags(Qt::Dialog);
	help->setReadOnly(true);
	help->resize(400,600);
	QFile file(":/txt/help");
	QTextStream stream(&file);
	file.open(QIODevice::ReadOnly);
	help->setHtml(stream.readAll());
	file.close();
	help->show();
}

void MainWindow::onAutoEndTurn()
{
	if (game->getActivePlayer()->isComputer()) {
		if (autoplay &&	!game->isActiveWinning()) {
			onEndTurn();
		} else {
			ui->endTurnButton->setEnabled(true);
			ui->editBoardButton->setEnabled(true);
		}
	} else { //człowiek -> hint
		ui->endTurnButton->setEnabled(true);
		ui->resetTurnButton->setEnabled(true);
		ui->redoTurnButton->setDisabled(true);
		ui->undoTurnButton->setDisabled(true);
		ui->saveGameButton->setDisabled(true);
	}
}

void MainWindow::onAutoplay()
{
	autoplay = !autoplay;
	ui->autoplayButton->setChecked(autoplay);
}

void MainWindow::onSaveGame()
{
	QString fileName = QFileDialog::getSaveFileName(this, "Zapisz stan gry", "diaballikSave.txt", ".txt");
	if (fileName.isNull())
		return;
	if (!game->writeToFile(fileName))
		QMessageBox::information(this, QString::fromUtf8("Coś poszło nie tak"), QString::fromUtf8("Nie udało się zapisać gry"));
}

void MainWindow::onLoadGame()
{
	QString fileName = QFileDialog::getOpenFileName(this, QString::fromUtf8("Otwórz plik ze stanem gry"), QDir::currentPath(), "");
	if (fileName.isEmpty())
		return;
	if (!game->loadFromFile(fileName)) {
		QMessageBox::information(this, QString::fromUtf8("Coś poszło nie tak"),
								 QString::fromUtf8("Nie udało się wczytać gry. Nastąpi reset planszy."));
		game->reset();
	}
	choosePlayers(false);
	game->makeTurn();
	defaultSettings();
	ui->undoTurnButton->setEnabled(game->isUndoAvailable());
	undoShortcut->blockSignals(game->isUndoAvailable());
}

void MainWindow::onEditBoard()
{
	if (edited) { //zakończ edycję
		if (game->isActiveWinning()) {
			QMessageBox::information(this, QString::fromUtf8("Nie ładnie tak"),
									 QString::fromUtf8("Ustawienie układu wygrywającego jest niedozwolone."));
			return;
		}
		game->finishEdit();
		ui->editBoardButton->setText(QString::fromUtf8("&Edytuj Grę"));
		edited = false;
		makeNewTurn();
	} else { //rozpocznij edycję
		if (QMessageBox::No == QMessageBox::question(this, "Edycja planszy",
				QString::fromUtf8("Edycja planszy spowoduje usunięcie historii. Czy na pewno chcesz kontynuować?"),
				QMessageBox::Yes|QMessageBox::No))
			return;
		game->startEdit();
		refreshStatusBar();
		ui->endTurnButton->setEnabled(true);
		ui->undoTurnButton->setDisabled(true);
		undoShortcut->blockSignals(true);
		ui->redoTurnButton->setDisabled(true);
		ui->resetTurnButton->setDisabled(true);
		ui->editBoardButton->setText(QString::fromUtf8("&Graj"));
		ui->hintButton->setDisabled(true);
		edited = true;
	}
}

void MainWindow::onHint()
{
	ui->hintButton->setDisabled(true);
	game->playHint();
}

MainWindow::~MainWindow()
{
	delete ui;
	delete game;
	delete scene;
	delete undoShortcut;
}
