#ifndef BOARDLOGIC_HPP
#define BOARDLOGIC_HPP

#include <color.hpp>
#include "turn.hpp"

class BoardModel
{
public:
	static const int SIZE = 7;
	enum direction {
		UP = 0,
		DOWN = 1,
		LEFT = 3,
		RIGHT = 4,
		LAST = 5
	};
	/** Oblicza wiersz danego pola.
	  * Gdy nieprawidłowy parametr, zwraca -1.
	  */
	static int row(int field);
	/** Oblicza kolumnę danego pola.
	  * Gdy nieprawidłowy parametr, zwraca -1.
	  */
	static int col(int field);
	/** Oblicza wartość pola na podstawie wiersza i kolumny.
	  * Gdy nieprawidłowy parametr, zwraca -1.
	  */
	static int field(int row, int col);
	/** Zwraca sąsiednie pole do danego w kierunku d
	  * Gdy nieprawidłowy parametr, zwraca -1.
	  */
	static int shift(int field, direction d);
	BoardModel();
	BoardModel(const BoardModel &bm);
	/** Zwalnia pamięć po bieżącej turze
	  */
	~BoardModel();
	/** Wykonuje ruch (przesunięcie lub rzut),
	  * sprawdzając jego poprawność
	  */
	bool move(int fieldFrom, int fieldTo);
	/** Wykonuje rzut piłką, sprawdzając jego poprawność.
	  */
	bool throwBall(int fieldFrom, int fieldTo, bool track = true);
	/** Wykonuje przesunięcie pionka,
	  * sprawdzając jego poprawność.
	  */
	bool shiftPawn(int fieldFrom, int fieldTo, bool track = true);
	/** Sprawdza czy dany rzut jest poprawny.
	  */
	bool isValidThrow(int fieldFrom, int fieldTo);
	/** Sprawdza czy dane przesunięcie jest poprawne.
	  */
	bool isValidShift(int fieldFrom, int fieldTo);
	/** Sprawdza czy gracz danego koloru posiada układ wygrywający.
	  */
	bool isWinning(Color::type color);
	/** Przekazuje bieżącą turę.
	  */
	Turn* getCurrentTurn();
	/** Rozpoczyna nową turę.
	  * Uwaga: Nie zwalnia pamięci po poprzedniej.
	  * Powinna to robić klasa korzystająca z tur (Game).
	  */
	void newTurn();
	/** Zwraca kolor aktywanego gracza
	  */
	Color::type getActiveColor();
	/** Przełącza aktywnego gracza
	  */
	void changeActiveColor();
	/** Zwraca tablicę wszystkich pionków.
	  * (najpierw białe, potem czarne)
	  */
	int *getPawns();
	/** Zwraca tablicę pionków danego koloru.
	  */
	int *getPawns(Color::type color);
	/** Podaje pole na którym znajduje się dana piłka
	  */
	int getBallField(Color::type color);
	/** Ustawia właściciela białej piłki.
	  * 0 <= i < SIZE
	  */
	void setWhiteBallOwner(int i);
	/** Ustawia właściciela czarnej piłki.
	  * 0 <= i < SIZE
	  */
	void setBlackBallOwner(int i);
	/** Doprowadza planszę do stanu początkowego.
	  */
	void reset();

private:
	/**
	  * indeksy od 0 do SIZE - 1 to białe
	  * indeksy od SIZE do 2 * SIZE - 1 to czarne
	  */
	int pawns[2 * SIZE];
	int ballWhiteOwnerIndex;
	int ballBlackOwnerIndex;
	Color::type activeColor;
	Turn* currentTurn;
	static bool areTheSameColor(int index1, int index2);
	/** Zwraca pole oddalone o rows wierszy i cols kolumn
	  * od zadanego.
	  */
	static int nextField(int fieldStart, int rows, int cols);
	/** Zwraca indeks pionka w tablicy pawns
	  * znajdującego się na zadanym polu.
	  * Jeśli pole jest puste zwraca -1.
	  */
	int getPawnIndex(int field);
	/** Pomocnicza dla publicznej
	  */
	bool isWinning(int thisStart, int thisEnd, int otherStart, int otherEnd, direction d, int ballOwner);
};

#endif // BOARDLOGIC_HPP
