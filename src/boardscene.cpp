#include "boardscene.hpp"
#include <QPixmap>

BoardScene::BoardScene(qreal size, int offsetX, int offsetY, int field, QPixmap pixmap, QObject *parent) :
	QGraphicsScene(0, 0, size, size, parent), offsetX(offsetX), offsetY(offsetY), field(field), pixmap(pixmap)
{
}

QPixmap BoardScene::getPixmap() const
{
	return pixmap;
}

int BoardScene::getOffsetX() const
{
	return offsetX;
}

int BoardScene::getOffsetY() const
{
	return offsetY;
}

int BoardScene::getField() const
{
	return field;
}

void BoardScene::drawBackground(QPainter *painter, const QRectF &rect)
{
	painter->drawPixmap(0, 0, pixmap.scaled(width(), height()));
}
