#ifndef BALL_HPP
#define BALL_HPP

#include <QGraphicsPixmapItem>
#include <QPropertyAnimation>
#include "pawn.hpp"
#include "color.hpp"

//class Color;
class Pawn;

class Ball : public QObject, public QGraphicsPixmapItem
{
	Q_OBJECT
	Q_PROPERTY(QPointF position READ pos WRITE setPos)
public:
	Ball(Color::type color, QPointF posPoint, qreal size);
	/** Zwraca właściciela piłki
	  */
	Pawn *getOwner() const;
	/** Zwraca kolor piłki
	  * (powinien się zgadzać z kolorem właściciela)
	  */
	Color::type getColor() const;
	/** Ustawia właściciela piłki
	  */
	void setOwner(Pawn *owner);
	/** Usuwa piłkę poprzedniemu właścicielowi i
	  * ustawia sobie nowego właściciela.
	  * (nowy właściciel musi sam zadbać o wręczenie sobie piłki)
	  */
	void throwTo(Pawn *pawn);
	/** Animuje i zmienia pozycję piłki
	  */
	void animate(QPointF posPoint);
	/** Zmienia pozycję piłki
	  * (bez animacji)
	  */
	void setPos(const QPointF &pos);
	/** Zwraca pozycję piłki.
	  */
	QPointF pos();

signals:
	void animationStarted();
	void animationFinished();

private slots:
	void onAnimationFinished();

private:
	QPixmap image;
	Pawn *owner;
	qreal size;
	Color::type color;
	/** przerabia pozycję pola na piłki
	  * (piłka zaczyna się na prawo i w dół względem pola
	  */
	QPointF makeBallPos(QPointF posPoint) const;
};

#endif // BALL_HPP
