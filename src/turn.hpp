#ifndef TURN_HPP
#define TURN_HPP

#include<vector>

class Turn
{
public:
	static const int MAX_SHIFTS = 2;
	static const int MAX_THROWS = 1;
	Turn();
	~Turn();
	/** Inicjuje iterator i jego kierunek.
	  * (ustawia przed wektorem ruchów)
	  */
	void setForwardIterator();
	/** Inicjuje iterator i jego kierunek.
	  * (ustawia za wektorem ruchów)
	  */
	void setBackwardIterator();
	/** Podaje początkowe pole ruchu dla bieżącej pozycji iteratora.
	  */
	int getStartField();
	/** Podaje końcowe pole ruchu dla bieżącej pozycji iteratora.
	  */
	int getEndField();
	/** Przesuwa iterator do następnego ruchu
	  * zgodnie z zainicjowanym wcześniej kierunkiem iteracji.
	  */
	void nextMove();
	/** Czy w turze istnieje następny ruch?
	  */
	bool hasNextMove();
	/** Dodaje przesunięcie do tury.
	  * (zwiększa licznik przesunięć)
	  */
	void addShift(int fieldFrom, int fieldTo);
	/** Dodaje rzut do tury.
	  * (zwiększa licznik rzutów)
	  */
	void addThrow(int fieldFrom, int fieldTo);
	/** Ile jeszcze można wykonać przesunięć w tej turze?
	  */
	int getShiftsAvailable();
	/** Ile jeszcze można wykonać rzutów w tej turze?
	  */
	int getThrowsAvailable();
	/** Czyści turę z ruchów.
	  */
	void clear();
	/** Dodaje ruch do tury nie zwiększając żadnych liczników
	  */
	void addMove(int fieldFrom, int fieldTo);

private:
	int iterator;
	std::vector<int> fields;
	/** licznik przesunięć */
	int shifts;
	/** licznik rzutów */
	int throws;
	/** bieżący kierunek iteracji */
	bool forward;
};

#endif // TURN_HPP
