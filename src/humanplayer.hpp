#ifndef HUMANPLAYER_HPP
#define HUMANPLAYER_HPP

#include "player.hpp"
#include "pawn.hpp"

class HumanPlayer : public Player
{
public:
	HumanPlayer(Color::type color);
	/** Nic nie robi, gra domyślnie jest w trybie
	  * odbierania kliknięć na scenie
	  */
	void makeTurn();
};

#endif // HUMANPLAYER_HPP
