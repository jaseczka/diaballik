#include "ai.hpp"
#include <algorithm>
#include <climits>
#include <iostream>

AI::AI(Color::type activeColor) : maxColor(activeColor)
{

}

int AI::Alfabeta(BoardModel *bm, int alfa, int beta, int counter)
{
	int result = 0;
	if (bm->isWinning(Color::WHITE) || bm->isWinning(Color::BLACK) ||
			counter >= MAX_DEPTH) {
		result = getBoardValue(bm);
		return result;
	}
	std::vector<BoardModel*> *sons = getSons(*bm);
	if (bm->getActiveColor() == maxColor) {
		result = INT_MIN;
		for (int i = 0; i < sons->size(); ++i) {
			int x = Alfabeta(sons->at(i), std::max(result, alfa), beta, counter + 1);
			if (x >= beta) {
				deleteSons(sons);
				return x;
			}
			result = std::max(result, x);
		}
	} else { //bm jest min
		result = INT_MAX;
		for (int i = 0; i < sons->size(); ++i) {
			int x = Alfabeta(sons->at(i), alfa, std::min(result, beta), counter +1);
			if (x <= alfa) {
				deleteSons(sons);
				return x;
			}
			result = std::min(result, x);
		}
	}
	deleteSons(sons);
	return result;
}

Turn *AI::getNextTurn(BoardModel &bm)
{
	Turn* turn = NULL;
	int movesLeft = bm.getCurrentTurn()->getShiftsAvailable() +
			bm.getCurrentTurn()->getThrowsAvailable();
	if (movesLeft <= 0)
		return NULL;
	std::vector<BoardModel*> *sons = new std::vector<BoardModel*>();
	std::vector<BoardModel*> *tmp = NULL;
	sons->push_back(&bm);
	int maxSonIndex = 0;
	int maxValue = INT_MIN;
	while (movesLeft > 0) {
		sons = getSons(*sons->at(maxSonIndex), false);
		if (tmp != NULL)
			deleteSons(tmp);
		std::random_shuffle(sons->begin(), sons->end());
		maxValue = INT_MIN;
		maxSonIndex = -1;
		for (int i = 0; i < sons->size(); ++i) {
			int value = Alfabeta(sons->at(i), INT_MIN, INT_MAX, Turn::MAX_SHIFTS + Turn::MAX_THROWS - movesLeft + 1);
			if (value == INT_MAX && sons->at(i)->isWinning(maxColor)) {
				turn = new Turn(*sons->at(i)->getCurrentTurn());
				deleteSons(sons);
				return turn;
			}
			if (value > maxValue) {
				maxValue = value;
				maxSonIndex = i;
			}
		}

		if (maxSonIndex < 0) {
			deleteSons(sons);
			return turn;
		}

		if (turn != NULL)
			delete turn;
		turn = new Turn(*sons->at(maxSonIndex)->getCurrentTurn());
		movesLeft = sons->at(maxSonIndex)->getCurrentTurn()->getShiftsAvailable() +
				sons->at(maxSonIndex)->getCurrentTurn()->getThrowsAvailable();
		tmp = sons;

	}
	deleteSons(sons);
	return turn;
}

int AI::getBoardValue(BoardModel *bm)
{
	Color::type activeColor = bm->getActiveColor();
	if (bm->isWinning(activeColor))
		return INT_MAX;
	if (bm->isWinning(Color::other(activeColor)))
		return INT_MIN;
	int boardValue = 0;
	int *pawns = bm->getPawns(activeColor);
	if (activeColor == Color::WHITE) {
		for (int i = 0; i < BoardModel::SIZE; ++i) {
			boardValue += BoardModel::row(pawns[i]);
		}
		boardValue += BoardModel::row(bm->getBallField(Color::WHITE));
	} else {
		for (int i = 0; i < BoardModel::SIZE; ++i) {
			boardValue += BoardModel::SIZE - BoardModel::row(pawns[i]) - 1;
		}
		boardValue += BoardModel::SIZE - BoardModel::row(bm->getBallField(Color::BLACK)) - 1;
	}
	return boardValue;
}

std::vector<BoardModel*> *AI::getSons(BoardModel &bm, bool mayChange)
{
	std::vector<BoardModel*> *sons = new std::vector<BoardModel*>();
	Turn *turn = bm.getCurrentTurn();
	if (turn->getShiftsAvailable() > 0) {
		addShifts(bm, sons);
	}
	if (turn->getThrowsAvailable() > 0) {
		addThrows(bm, sons);
	}
	if (sons->empty() && mayChange) {
		//zmień turę/gracza
		addOpponentMoves(bm, sons);
	}
	return sons;
}

void AI::addShifts(BoardModel &bm, std::vector<BoardModel*> *sons)
{
	Color::type activeColor = bm.getActiveColor();
	int *pawns = bm.getPawns(activeColor);
	for (int i = 0; i < BoardModel::SIZE; ++i) {
		for (int d = 0; d < BoardModel::LAST; ++d) {
			int fieldTo = BoardModel::shift(pawns[i], BoardModel::direction(d));
			if (bm.isValidShift(pawns[i], fieldTo)) {
				BoardModel *son = new BoardModel(bm);
				son->shiftPawn(pawns[i], fieldTo);
				sons->push_back(son);
			}
		}
	}
}

void AI::addOpponentMoves(BoardModel bm, std::vector<BoardModel*> *sons)
{
	delete bm.getCurrentTurn();
	bm.newTurn();
	bm.changeActiveColor();
	addShifts(bm, sons);
	addThrows(bm, sons);
}

void AI::addThrows(BoardModel &bm, std::vector<BoardModel*> *sons)
{
	Color::type activeColor = bm.getActiveColor();
	int *pawns = bm.getPawns(activeColor);
	int ballField = bm.getBallField(activeColor);
	for (int i = 0; i < BoardModel::SIZE; ++i) {
		int fieldTo = pawns[i];
		if (bm.isValidThrow(ballField, fieldTo)) {
			BoardModel *son = new BoardModel(bm);
			son->throwBall(ballField, fieldTo);
			sons->push_back(son);
		}
	}
}

void AI::deleteSons(std::vector<BoardModel*> *sons)
{
	for (int i = 0; i < sons->size(); ++i) {
		delete sons->at(i);
	}
	delete sons;
}
