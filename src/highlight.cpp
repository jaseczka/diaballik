#include <iostream>

#include "highlight.hpp"
#include <QPainter>
#include <QBrush>
#include <QPen>
#include <QGraphicsSceneMouseEvent>

Highlight::Highlight(int field, QPointF posPoint, int size, QObject *parent) :
	QObject(parent), field(field), size(size), pressed(false)
{
	this->rect = QRectF(posPoint.rx(), posPoint.ry(), size, size);
	setFlags(QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsFocusable);
}

QRectF Highlight::boundingRect() const
{
	return rect;
}

QPainterPath Highlight::shape() const
{
	QPainterPath p = QPainterPath(pos());
	p.addRect(rect);
	return p;
}

void Highlight::paint(QPainter *painter, const QStyleOptionGraphicsItem * /* option */, QWidget * /* widget */)
{
	painter->setPen(QPen(Qt::transparent));
	painter->drawRect(rect);
	painter->drawPixmap(rect.toRect(), QPixmap(":/img/highlight").scaledToHeight(size));
}

void Highlight::setField(int field)
{
	this->field = field;
}

int Highlight::getField()
{
	return field;
}

void Highlight::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	pressed = true;
	QGraphicsItem::mousePressEvent(event);
}

void Highlight::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsItem::mouseReleaseEvent(event);
	if(pressed)	{
		emit clicked(field);
	}
	pressed = false;

}
