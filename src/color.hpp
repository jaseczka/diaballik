#ifndef COLOR_HPP
#define COLOR_HPP

class Color
{
public:
	enum type {
		WHITE,
		BLACK
	};
	/** Zwraca ten drugi kolor
	  */
	static type other(type color);
};

#endif // COLOR_HPP
