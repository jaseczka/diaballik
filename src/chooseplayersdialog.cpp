#include "chooseplayersdialog.hpp"
#include "ui_chooseplayersdialog.h"

#include <QPushButton>

ChoosePlayersDialog::ChoosePlayersDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::ChoosePlayersDialog)
{
	ui->setupUi(this);
	ui->whiteHumanRadio->setChecked(true);
	ui->blackHumanRadio->setChecked(true);
}

ChoosePlayersDialog::~ChoosePlayersDialog()
{
	delete ui;
}

bool ChoosePlayersDialog::isWhiteHuman()
{
	return whiteHuman;
}

bool ChoosePlayersDialog::isBlackHuman()
{
	return blackHuman;
}

void ChoosePlayersDialog::setNotRejectable()
{
	ui->buttonBox->button(QDialogButtonBox::Cancel)->setDisabled(true);
}

void ChoosePlayersDialog::accept()
{
	whiteHuman = ui->whiteHumanRadio->isChecked();
	blackHuman = ui->blackHumanRadio->isChecked();
	QDialog::accept();
}
